package healingvisualization.androidhive.cardview;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ajay on 4/23/2017.
 *
 *     {
 "English_songs": [],
 "Hindi_songs": [],
 "album_id": "701",
 "album_cover": "NotFound",
 "Telugu_songs": [
 {
 "song_id": "701.Telugu.1",
 "song_name": "0-Acne Understanding and Visualization-Telugu.mp3",
 "song_desc_short": "Explains Who/what is the reason for Acne.",
 "song_title": "Introduction for Acne visualization.",
 "song_desc_long": "",
 "song_status": true
 },
 {
 "song_id": "701.Telugu.2",
 "song_name": "1-Acne Healing - Telugu-Full.mp3",
 "song_desc_short": " Listen to this detailed visualization daily twice for quick result.",
 "song_title": "Long Acne Healing Visualization",
 "song_desc_long": "",
 "song_status": true
 },
 {
 "song_id": "701.Telugu.3",
 "song_name": "2-Acne Healing - Telugu - Short.mp3",
 "song_desc_short": "Listen to this short visualization daily twice if you don't have enough time.",
 "song_title": "2-Acne Healing - Telugu - Short.mp3",
 "song_desc_long": "",
 "song_status": true
 }
 ],
 "album_status": true,
 "album_name": "Acne"
 },

 *
 */


class albums_singleton {
    private static final albums_singleton ourInstance = new albums_singleton();
    public ArrayList<String> song_names = new ArrayList<String>();
    public JSONObject album_single = new JSONObject();
    public JSONArray album_songs = new JSONArray();
    private static String bucket = "https://storage.googleapis.com/healyourself01/Healing%20Finalized%20Audios/";
    public String getSelected_language() {
        return selected_language;
    }

    public void setSelected_language(String selected_language) {
        this.selected_language = selected_language;
    }

    private String selected_language = "Telugu";
    public String getAlbumid_selected() {
        return albumid_selected;
    }

    public void setAlbumid_selected(String albumid_selected) {
        this.albumid_selected = albumid_selected;
    }

    public String getSongid_selected() {
        return songid_selected;
    }

    public void setSongid_selected(String songid_selected) {
        this.songid_selected = songid_selected;
    }

    public String albumid_selected = "";
    public String songid_selected = "";

    public JSONArray getAlbums() {
        return albums;
    }

    public void setAlbums(JSONArray albums) {
        this.albums = albums;
    }

    public JSONArray albums;
    static albums_singleton getInstance() {
        return ourInstance;
    }

    private albums_singleton() {
    }
/*This method returns a single JSON object from the albums JSON array list*/
    public JSONObject get_single_album(int index){

        try{
            JSONObject album_single = new JSONObject();/*Initialize with null to clear old data*/
            album_single = albums.getJSONObject(index);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return album_single;
    }
/*This method returns a single JSON object from the albums JSON array list*/

    /*********************************************************
     * get_song_names
     * Returns the list of songs with index and the language
     * @param index, Language
     * @return song_names
     *********************************************************/
    public ArrayList<String> get_song_names(int index,String Language){
                                /*Loop through the JSON Array*/
        try {
            song_names.clear();/*Initialize with null to clear old data*/
            JSONObject object0 = get_JSONObj_selected();
            ArrayList<String> songs_list = new ArrayList<String>();
            JSONArray album_songs = object0.getJSONArray(Language + "_songs");
            for(int i=0;i<album_songs.length();i++)
            {
                JSONObject object1 = album_songs.getJSONObject(i);
                String song_name = object1.getString("song_name");
                /*Check the status of song*/
                if (object1.getString("song_status").equals("true"))
                {
                    song_names.add(song_name);
                }
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return song_names;
    }
    /*This method returns the "album_songs" for the selected album*/
    public JSONArray get_album_songs(int index){
        try{
             album_songs = new JSONArray();/*Initialize with null to clear old data*/
            album_songs = get_JSONObj_selected().getJSONArray( selected_language + "_songs");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return album_songs;
    }
    /*Returns the album_folder for the selected album*/
    public String get_album_folder (){
        String folder = "";
        try {
         folder = get_JSONObj_selected().getString("album_name");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return folder;
    }

    /*Returns the selected song name for the selected song id*/

    /*************************************************************
     * String get_selected_song_name
     * @return Selected Song
     *************************************************************/
    public String get_selected_song_name (){
        String selected_song_name = "";
        try {
            JSONArray obj4 = get_JSONObj_selected().getJSONArray(selected_language + "_songs");

            for(int i =0; i<obj4.length();i++)
            {
                JSONObject object4 = album_songs.getJSONObject(i);
                String song_id = object4.getString("song_id");
                if (song_id == songid_selected){
                    selected_song_name = object4.getString("song_name");
                }
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return selected_song_name;
    }

    /*****************************************************************
     * get_album_cover
     * returns the album cover for the selected album.
     * @return album_cover
     *****************************************************************/
    public String get_album_cover_url(){
        String album_cover = "";
        try {

            album_cover =
                    bucket+get_JSONObj_selected().getString("album_name").replace(" ","%20")
                            +"/"+
                    get_JSONObj_selected().getString("album_cover").replace(" ", "%20");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return album_cover;
    }

    /*****************************************************************
     * get_song_title
     * returns the album title for the selected album.
     * @return album_title
     *****************************************************************/
    public String get_song_title(){
        String song_title = "";
        try {

            song_title = get_selected_song_obj().getString("song_title");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return song_title;
    }
    /*****************************************************************
     * get_song_desc_short
     * returns the song short desc for the selected album.
     * @return song_desc_short
     *****************************************************************/
    public String get_song_desc_short(){
        String song_desc_short = "";
        try {
            song_desc_short = get_selected_song_obj().getString("song_desc_short");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return song_desc_short;
    }
    /*****************************************************************
     * get_selected_song_obj
     * returns the selected song obj
     * @return selected song obj
     *****************************************************************/
    public JSONObject get_selected_song_obj(){
        JSONObject selected_song = new JSONObject();
        try {

            JSONArray tmp5 = get_JSONObj_selected().getJSONArray(selected_language+"_songs");

            for (int i=0;i<tmp5.length();i++) {
                JSONObject tmp_song = tmp5.getJSONObject(i);
                if (tmp_song.getString("song_id") == songid_selected) {
                    selected_song = tmp_song;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return selected_song;

    }

    /*This method returns the "album_songs" for the given index*/
    public JSONObject get_JSONObj_selected(){
        JSONObject jsonobj_selected = new JSONObject();
        try{

            for(int i = 0; i<albums.length();i++)
            {
                jsonobj_selected = albums.getJSONObject(i);
                if(jsonobj_selected.getString("album_id") == albumid_selected)
                { break;}
           }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return jsonobj_selected;
    }
}

