package healingvisualization.androidhive.cardview;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by mindgame on 3/11/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {
    private Context mContext;

    private ArrayList<AlbumObject> arrayList=new ArrayList<>();


    public RecyclerAdapter(Context mContext,ArrayList<AlbumObject> arrayList) {
        this.arrayList = arrayList;
        this.mContext = mContext;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_item,parent,false);
        RecyclerViewHolder recyclerViewHolder=new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {

        final AlbumObject albumObject=arrayList.get(position);
        holder.textView1.setText(albumObject.getTitle());
        holder.textView2.setText(albumObject.getDes());
        holder.btnitem_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*************************change here to AlbumsAdapter.cladss*************************************/
                Intent i = new Intent(mContext.getApplicationContext(),AndroidDownloadFileByProgressBarActivity.class);
                Bundle b = new Bundle();
                b.putInt("flag",position);
                String songid = arrayList.get(position).getSongid();
                albums_singleton.getInstance().setSongid_selected(songid);
//                Toast.makeText(mContext,"SognId :"+songid,Toast.LENGTH_SHORT).show();
                i.putExtras(b);
                mContext. startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
    public class RecyclerViewHolder extends RecyclerView.ViewHolder{

        Button btnitem_play;
        TextView textView1,textView2;

        public RecyclerViewHolder(View view) {
            super(view);

            textView1= (TextView) view.findViewById(R.id.text_view1);
            textView2= (TextView) view.findViewById(R.id.text_view2);
            btnitem_play=(Button)view.findViewById(R.id.item_play);
        }
    }
}
