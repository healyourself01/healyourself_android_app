package healingvisualization.androidhive.cardview;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.R.attr.key;

public class MainActivity extends AppCompatActivity {
//RECYCLER VIEW OBJECT DECLERATION
    private RecyclerView recyclerView;
    //OBJECT FOR THE ALBUMSADAPTER CLASS
    private AlbumsAdapter adapter;
    //PASSING THE ALBUM CLASS AS A PARAMETER TO THE LIST
    private List<Album> albumList;
    JSONArray albums;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initCollapsingToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//INITIALIZING THE LIST OBJECT AND CALLING ALBUMADAPTER CLASS
        albumList = new ArrayList<>();
        adapter = new AlbumsAdapter(this, albumList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);


               /*VOLLEY TESTING*/
        // Instantiate the RequestQueue.
        // Get the albums JSON using volley library.
        // then load the data into singleton class.
        // the access the data with in the app anyware.

        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://healyourself-firebase.firebaseio.com/albums.json";


// Request a string response from the provided URL.
        JsonArrayRequest jsArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Response: " , response.toString());
                        albums_singleton.getInstance().setAlbums(response);

                        albums = albums_singleton.getInstance().getAlbums();
                        Log.d("data from singleton:", albums.toString());
                        prepareAlbums();


                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });
// Add the request to the RequestQueue.
        queue.add(jsArrayRequest);




        try {
            Glide.with(this).load(R.drawable.card_banner).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));

                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /**
     * Adding few albumitem for testing
     */
    private void prepareAlbums() {
        String cover  = "https://storage.googleapis.com/healyourself01/Healing%20Finalized%20Audios/Aashiqui2(2013)/Aashiqui2_album_imgae.jpg";
        String bucket = "https://storage.googleapis.com/healyourself01/Healing%20Finalized%20Audios/";
        /********************Load the Images into the Album********************/
        try {

            /*Add the Introduction at first*/
            for(int i=0;i<albums.length();i++)
            {
                JSONObject object3 = albums.getJSONObject(i);
                String albumid = object3.getString("album_id");
                String title1 = object3.getString("album_name");
                String album_status = object3.getString("album_status");
                int count = object3.getJSONArray("Telugu_songs").length();
                String thumbnail1 = bucket+object3.getString("album_name")
                        +"/"+object3.getString("album_cover");
                Album b = new Album(albumid,title1 , count, thumbnail1);
                if (album_status.equals ("true") && title1.equals("Introduction")){
                    albumList.add(b);}
            }

            /*Add the remaining items later*/
            for(int i=0;i<albums.length();i++)
            {
                JSONObject object3 = albums.getJSONObject(i);
                String albumid = object3.getString("album_id");
                String title1 = object3.getString("album_name");
                String album_status = object3.getString("album_status");
                int count = object3.getJSONArray("Telugu_songs").length();
                String thumbnail1 = bucket+object3.getString("album_name")
                        +"/"+object3.getString("album_cover");
                Album b = new Album(albumid,title1 , count, thumbnail1);
                if (album_status.equals ("true") && !title1.equals("Introduction") ){
                albumList.add(b);}
            }
        }
        catch (Exception e){
            e.printStackTrace();

        }

        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

      /*
       * Intent intent = new Intent(Intent.ACTION_MAIN);
       * intent.addCategory(Intent.CATEGORY_HOME);
       * intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       * startActivity(intent);
       */

            Intent i = new Intent(MainActivity.this, ContentPage.class);
            startActivity(i);
        }

        return super.onKeyDown(keyCode, event);
    }
}
