package healingvisualization.androidhive.cardview;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class PrivacyPolicy extends Activity {
    private WebView mWebview ;
    Button btn_start;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        //btn_start=(Button)findViewById(R.id.btn_start);
        //btn_start.setVisibility(View.GONE);

        mWebview  = new WebView(this);
        mWebview=(WebView)findViewById(R.id.web_view1) ;

        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript

        final Activity activity = this;

        mWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }
        });

        mWebview .loadUrl("http://mindgamesoft.in/privacy.html");

    }

}
