package healingvisualization.androidhive.cardview;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;

import java.util.ArrayList;

public class Album_listItem extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    ImageView album_image;
    TextView title;



    ArrayList<AlbumObject>arrayList=new ArrayList<>();
    int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_list_item);

        Bundle b= getIntent().getExtras();
        position=b.getInt("flag");

        recyclerView= (RecyclerView) findViewById(R.id.recyclerview);
        /*albumimage*/
        album_image=(ImageView)findViewById(R.id.img_view);
        title=(TextView)findViewById(R.id.title);
        title.setText(albums_singleton.getInstance().get_album_folder());
        Glide.with(getApplicationContext()).load(albums_singleton.getInstance().get_album_cover_url()).into(album_image);



        /*-----------Load the list of songs from album JSON using the index------------*/
        JSONArray album_songs = new JSONArray();
        album_songs = albums_singleton.getInstance().get_album_songs(position);
        arrayList.clear();

        /*Todo - Need to clear the adaptor and set blank screen. data from prev session shown*/
        if (album_songs != null && album_songs.length()>0) {
/* SONGS JSON Format
           {
                "song_id": "703.English.1",
                "song_name": "01-BP-English-Long-10min.mp3",
                "song_desc_short": "Listen to this detailed visualization daily 2 times for 21 days.",
                "song_title": "Blood Pressure Healing Detailed Visualization.",
                "song_desc_long": "",
                "song_status": true
            }

* */

            for (int i = 0; i < album_songs.length(); i++) {
                try {
                    String songid = album_songs.getJSONObject(i).getString("song_id");
                    String title = album_songs.getJSONObject(i).getString("song_title");
                    String desc = album_songs.getJSONObject(i).getString("song_desc_short");
                    String song_status = album_songs.getJSONObject(i).getString("song_status");

                    if (song_status.equals("true")) {
                        AlbumObject dataProvider = new AlbumObject(songid, title, desc);
                        arrayList.add(dataProvider);
                    }

                } catch (Exception e) {
                    Log.d("ERROR", "Album not found for index");
                    e.printStackTrace();
                }
            }
            set_songs_to_screen();

        }
        else {
            Log.d("ERROR", "Album not found for index");
//            Toast.   ( "error loading the songs",Toast.LENGTH_LONG);
        }



    }
    /*Set the songs to screen*/
public void set_songs_to_screen(){
    adapter = new RecyclerAdapter(this, arrayList);
    recyclerView.setHasFixedSize(true);
    layoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setAdapter(adapter);

}
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

      /*
       * Intent intent = new Intent(Intent.ACTION_MAIN);
       * intent.addCategory(Intent.CATEGORY_HOME);
       * intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       * startActivity(intent);
       */

            Intent i = new Intent(Album_listItem.this, MainActivity.class);
            startActivity(i);
        }

        return super.onKeyDown(keyCode, event);
    }
}
