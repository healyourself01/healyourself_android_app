package healingvisualization.androidhive.cardview;

/**
 * Created by mindgame on 3/11/2017.
 */

public class AlbumObject {


    String songid = "";

    String title,des;

    public AlbumObject( String songid, String title, String des) {
        this.songid = songid;
        this.setTitle(title);
        this.setDes(des);
    }
    public String getSongid() {
        return songid;
    }

    public void setSongid(String songid) {
        this.songid = songid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getTitle() {
        return title;

    }

    public String getDes() {
        return des;
    }
}
