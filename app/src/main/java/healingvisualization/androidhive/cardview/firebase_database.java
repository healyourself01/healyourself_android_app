package healingvisualization.androidhive.cardview;

import android.provider.Settings;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by mindgame on 3/14/2017.
 */

public class firebase_database {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("albumitem");


    public void writetodb(){
        // Write a message to the database
//        myRef.setValue("Soma");
        Log.d("data", "writetodb - successfully!");
    }

    public void readfromdb(){
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                ArrayList <albumitem> albums_array = dataSnapshot.getValue(ArrayList.class);

                Log.d("data", "Albums from JSON is: " + albums_array);

//                for (JSONObject item:albums_array
//                     ) {
//                    try {
//                        Log.d("data", "------------------------firebase data read-------------------------------------------------------");
//                        Log.d("data", "Album Name from JSON is: " + item.getString("album-name"));
//                        Log.d("data", "----------------------------firebase data read------------------------------------");
//
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                }


            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("data", "Failed to read value.", error.toException());
            }
        });
    }
}
