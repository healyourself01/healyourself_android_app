package healingvisualization.androidhive.cardview;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class ContentPage extends Activity {
    //private WebView mWebview ;
    Button btn_start;
    TextView text_html,terms_privacy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_html);
        btn_start=(Button)findViewById(R.id.btn_start);
        text_html=(TextView)findViewById(R.id.txt_html) ;
        terms_privacy=(TextView)findViewById(R.id.privacy);
        terms_privacy.setPaintFlags(terms_privacy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        terms_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ContentPage.this, PrivacyPolicy.class);
                startActivity(i);
            }
        });


        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://storage.googleapis.com/healyourself01/Introduction.html";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        text_html.setText(Html.fromHtml(response));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                text_html.setText("Error Loading the Inroduction.html");
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);


        btn_start.setVisibility(View.VISIBLE);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(ContentPage.this,MainActivity.class);
                startActivity(i);
            }
        });

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {


        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
       startActivity(intent);
        }

        return super.onKeyDown(keyCode, event);
    }
}
