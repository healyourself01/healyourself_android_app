package healingvisualization.androidhive.cardview;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.util.Log;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by mindgame on 3/13/2017.
 */


 public class Notifications_local {

    private  Context context;


public  Notifications_local(Context context) {
this.context=context;
}


     public void notify_local(){

        Notification.Builder notify;
        NotificationManager nm;
        notify=new Notification.Builder(context);
        notify.setTicker("HealYourself");
        notify.setWhen(System.currentTimeMillis());
        notify .setPriority(Notification.PRIORITY_MAX);
        notify .setContentTitle("Content Title");
        notify .setContentText("Notification Content");
        notify .setSmallIcon(R.drawable.promo1);
        nm=(NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);


        Intent intent=new Intent();
        PendingIntent pendingIntent=PendingIntent.getActivity(context,0,intent,0);
        notify.addAction(R.drawable.video_player_btn_next_p,"",pendingIntent);
        nm.notify(0,notify.getNotification());


    };


    public static void  printlog(){
        Log.d("log","method called.");
    }

}

